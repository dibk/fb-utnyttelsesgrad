# FB-Utnyttelsesgrad #

Applikasjon for beregning av utnyttelsesgrad. Tilbyr API (json/xml) for å kvalitetssikre beregninger av utnyttelsesgrad i forbindelse med byggesøknader. 

URL: http://apibygg.dibk.no/utnyttelsesgrad