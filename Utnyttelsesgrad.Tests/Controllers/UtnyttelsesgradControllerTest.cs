﻿using DIBK.FellestjenesterBygg.Utnyttelsesgrad.Controllers;
using DIBK.FellestjenesterBygg.Utnyttelsesgrad.Models;
using DIBK.FellestjenesterBygg.Utnyttelsesgrad.Services;
using System.Collections.Generic;
using System.Linq;
using Utnyttelsesgrad.Controllers;
using Xunit;


namespace Utnyttelsesgrad.Tests.Controllers
{
    public class UtnyttelsesgradControllerTest
    {

        // =============== TESTS for api/Utnyttelsesgrad/BYA  ===============
        [Fact]
        public void Post_BYA_normal()
        {
            // Arrange
            ArealdisponeringUtenTomt arealdisponering = new ArealdisponeringUtenTomt();
            UtnyttelsesgradController utnyttelsesgradController = new UtnyttelsesgradController();

            // Populate input data for normal calculations
            arealdisponering.arealBebyggelseEksisterende = 123.56;
            arealdisponering.arealBebyggelseNytt = 23.66;
            arealdisponering.arealBebyggelseSomSkalRives = 12.5;
            arealdisponering.kravKvmPrParkeringsplass = 18.0;
            arealdisponering.antallParkeringsplasserTerreng = 2;


            // Act
            Utnyttingsgrad utnyttingsgrad = utnyttelsesgradController.PostBYA(arealdisponering);

            // Assert
            Assert.NotNull(utnyttingsgrad);
            Assert.Equal(utnyttingsgrad.beregnetGradAvUtnytting, 171.0);
            Assert.Equal(utnyttingsgrad.statusKode, Utnyttingsgrad.UTNYTTINGSGRAD_SUCCESS);
            Assert.Equal(utnyttingsgrad.enhetForBeregning, Constants.returned_unit_m2);
            Assert.Contains(utnyttingsgrad.resultatBeskrivelse, Constants.resultatBeskrivelseBYA);
        }



        [Fact]
        public void Post_BYA_negativeValues()
        {
            // Arrange
            ArealdisponeringUtenTomt arealdisponering = new ArealdisponeringUtenTomt();
            UtnyttelsesgradController utnyttelsesgradController = new UtnyttelsesgradController();

            // Populate input data for normal calculations
            arealdisponering.arealBebyggelseEksisterende = 123.56;
            arealdisponering.arealBebyggelseNytt = 23.66;
            arealdisponering.arealBebyggelseSomSkalRives = -12.5;
            arealdisponering.kravKvmPrParkeringsplass = 18.0;
            arealdisponering.antallParkeringsplasserTerreng = 2;


            // Act
            Utnyttingsgrad utnyttingsgrad = utnyttelsesgradController.PostBYA(arealdisponering);

            // Assert
            Assert.NotNull(utnyttingsgrad);
            Assert.Equal(utnyttingsgrad.statusKode, Utnyttingsgrad.UTNYTTINGSGRAD_ERROR);
        }



        [Fact]
        public void Post_BYA_negativeResult()
        {
            // Arrange
            ArealdisponeringUtenTomt arealdisponering = new ArealdisponeringUtenTomt();
            UtnyttelsesgradController utnyttelsesgradController = new UtnyttelsesgradController();

            // Populate input data for normal calculations
            arealdisponering.arealBebyggelseEksisterende = 123.56;
            arealdisponering.arealBebyggelseNytt = 23.66;
            arealdisponering.arealBebyggelseSomSkalRives = 300.5;
            arealdisponering.kravKvmPrParkeringsplass = 18.0;
            arealdisponering.antallParkeringsplasserTerreng = 2;


            // Act
            Utnyttingsgrad utnyttingsgrad = utnyttelsesgradController.PostBYA(arealdisponering);

            // Assert
            Assert.NotNull(utnyttingsgrad);
            Assert.Equal(utnyttingsgrad.statusKode, Utnyttingsgrad.UTNYTTINGSGRAD_ERROR);
        }




        // =============== TESTS for api/Utnyttelsesgrad/BRA  ===============
        [Fact]
        public void Post_BRA_normal()
        {
            // Arrange
            ArealdisponeringUtenTomt arealdisponering = new ArealdisponeringUtenTomt();
            UtnyttelsesgradController utnyttelsesgradController = new UtnyttelsesgradController();

            // Populate input data for normal calculations
            arealdisponering.arealBebyggelseEksisterende = 123.56;
            arealdisponering.arealBebyggelseNytt = 23.66;
            arealdisponering.arealBebyggelseSomSkalRives = 12.5;
            arealdisponering.kravKvmPrParkeringsplass = 18.0;
            arealdisponering.antallParkeringsplasserTerreng = 2;


            // Act
            Utnyttingsgrad utnyttingsgrad = utnyttelsesgradController.PostBRA(arealdisponering);

            // Assert
            Assert.NotNull(utnyttingsgrad);
            Assert.Equal(utnyttingsgrad.beregnetGradAvUtnytting, 171.0);
            Assert.Equal(utnyttingsgrad.statusKode, Utnyttingsgrad.UTNYTTINGSGRAD_SUCCESS);
            Assert.Equal(utnyttingsgrad.enhetForBeregning, Constants.returned_unit_m2);
            Assert.Contains(utnyttingsgrad.resultatBeskrivelse, Constants.resultatBeskrivelseBRA);

        }



        [Fact]
        public void Post_BRA_negativeValues()
        {
            // Arrange
            ArealdisponeringUtenTomt arealdisponering = new ArealdisponeringUtenTomt();
            UtnyttelsesgradController utnyttelsesgradController = new UtnyttelsesgradController();

            // Populate input data for normal calculations
            arealdisponering.arealBebyggelseEksisterende = 123.56;
            arealdisponering.arealBebyggelseNytt = 23.66;
            arealdisponering.arealBebyggelseSomSkalRives = -12.5;
            arealdisponering.kravKvmPrParkeringsplass = 18.0;
            arealdisponering.antallParkeringsplasserTerreng = 2;


            // Act
            Utnyttingsgrad utnyttingsgrad = utnyttelsesgradController.PostBRA(arealdisponering);

            // Assert
            Assert.NotNull(utnyttingsgrad);
            Assert.Equal(utnyttingsgrad.statusKode, Utnyttingsgrad.UTNYTTINGSGRAD_ERROR);
        }



        [Fact]
        public void Post_BRA_negativeResult()
        {
            // Arrange
            ArealdisponeringUtenTomt arealdisponering = new ArealdisponeringUtenTomt();
            UtnyttelsesgradController utnyttelsesgradController = new UtnyttelsesgradController();

            // Populate input data for normal calculations
            arealdisponering.arealBebyggelseEksisterende = 123.56;
            arealdisponering.arealBebyggelseNytt = 23.66;
            arealdisponering.arealBebyggelseSomSkalRives = 300.5;
            arealdisponering.kravKvmPrParkeringsplass = 18.0;
            arealdisponering.antallParkeringsplasserTerreng = 2;


            // Act
            Utnyttingsgrad utnyttingsgrad = utnyttelsesgradController.PostBRA(arealdisponering);

            // Assert
            Assert.NotNull(utnyttingsgrad);
            Assert.Equal(utnyttingsgrad.statusKode, Utnyttingsgrad.UTNYTTINGSGRAD_ERROR);
        }




        // =============== TESTS for api/Utnyttelsesgrad/ProsentBYA  ===============
        [Fact]
        public void Post_pctBYA_normal()
        {
            // Arrange
            ArealdisponeringMedTomt arealdisponering = new ArealdisponeringMedTomt();
            UtnyttelsesgradController utnyttelsesgradController = new UtnyttelsesgradController();

            // Populate input data for normal calculations
            arealdisponering.tomtearealByggeomraade = 5000.34;
            arealdisponering.arealBebyggelseEksisterende = 200.56;
            arealdisponering.arealBebyggelseNytt = 50.66;
            arealdisponering.arealBebyggelseSomSkalRives = 22.5;
            arealdisponering.kravKvmPrParkeringsplass = 18.0;
            arealdisponering.antallParkeringsplasserTerreng = 4;


            // Act
            Utnyttingsgrad utnyttingsgrad = utnyttelsesgradController.PostProsentBYA(arealdisponering);

            // Assert
            Assert.NotNull(utnyttingsgrad);
            Assert.Equal(utnyttingsgrad.beregnetGradAvUtnytting, 7.0);
            Assert.Equal(utnyttingsgrad.statusKode, Utnyttingsgrad.UTNYTTINGSGRAD_SUCCESS);
            Assert.Equal(utnyttingsgrad.enhetForBeregning, Constants.returned_unit_percent);
            Assert.Contains(utnyttingsgrad.resultatBeskrivelse, Constants.resultatBeskrivelsePctBYA);
              
        }



        [Fact]
        public void Post_pctBYA_zeroTomt()
        {
            // Arrange
            ArealdisponeringMedTomt arealdisponering = new ArealdisponeringMedTomt();
            UtnyttelsesgradController utnyttelsesgradController = new UtnyttelsesgradController();

            // Populate input data for normal calculations
            arealdisponering.tomtearealByggeomraade = 0.0;
            arealdisponering.arealBebyggelseEksisterende = 200.56;
            arealdisponering.arealBebyggelseNytt = 50.66;
            arealdisponering.arealBebyggelseSomSkalRives = 22.5;
            arealdisponering.kravKvmPrParkeringsplass = 18.0;
            arealdisponering.antallParkeringsplasserTerreng = 4;


            // Act
            Utnyttingsgrad utnyttingsgrad = utnyttelsesgradController.PostProsentBYA(arealdisponering);

            // Assert
            Assert.NotNull(utnyttingsgrad);
            Assert.Equal(utnyttingsgrad.statusKode, Utnyttingsgrad.UTNYTTINGSGRAD_ERROR);
        }



        [Fact]
        public void Post_pctBYA_negativeInputParamter()
        {
            // Arrange
            ArealdisponeringMedTomt arealdisponering = new ArealdisponeringMedTomt();
            UtnyttelsesgradController utnyttelsesgradController = new UtnyttelsesgradController();

            // Populate input data for normal calculations
            arealdisponering.tomtearealByggeomraade = 3000.0;
            arealdisponering.arealBebyggelseEksisterende = -200.56;
            arealdisponering.arealBebyggelseNytt = 50.66;
            arealdisponering.arealBebyggelseSomSkalRives = 22.5;
            arealdisponering.kravKvmPrParkeringsplass = 18.0;
            arealdisponering.antallParkeringsplasserTerreng = 4;


            // Act
            Utnyttingsgrad utnyttingsgrad = utnyttelsesgradController.PostProsentBYA(arealdisponering);

            // Assert
            Assert.NotNull(utnyttingsgrad);
            Assert.Equal(utnyttingsgrad.statusKode, Utnyttingsgrad.UTNYTTINGSGRAD_ERROR);
        }



        [Fact]
        public void Post_pctBYA_negativeResult()
        {
            // Arrange
            ArealdisponeringMedTomt arealdisponering = new ArealdisponeringMedTomt();
            UtnyttelsesgradController utnyttelsesgradController = new UtnyttelsesgradController();

            // Populate input data for normal calculations
            arealdisponering.tomtearealByggeomraade = 3000.0;
            arealdisponering.arealBebyggelseEksisterende = 200.56;
            arealdisponering.arealBebyggelseNytt = 50.66;
            arealdisponering.arealBebyggelseSomSkalRives = 400.5;
            arealdisponering.kravKvmPrParkeringsplass = 18.0;
            arealdisponering.antallParkeringsplasserTerreng = 4;


            // Act
            Utnyttingsgrad utnyttingsgrad = utnyttelsesgradController.PostProsentBYA(arealdisponering);

            // Assert
            Assert.NotNull(utnyttingsgrad);
            Assert.Equal(utnyttingsgrad.statusKode, Utnyttingsgrad.UTNYTTINGSGRAD_ERROR);
        }




        // =============== TESTS for api/Utnyttelsesgrad/ProsentBRA  ===============
        [Fact]
        public void Post_pctBRA_normal()
        {
            // Arrange
            ArealdisponeringMedTomt arealdisponering = new ArealdisponeringMedTomt();
            UtnyttelsesgradController utnyttelsesgradController = new UtnyttelsesgradController();

            // Populate input data for normal calculations
            arealdisponering.tomtearealByggeomraade = 5000.34;
            arealdisponering.arealBebyggelseEksisterende = 200.56;
            arealdisponering.arealBebyggelseNytt = 50.66;
            arealdisponering.arealBebyggelseSomSkalRives = 22.5;
            arealdisponering.kravKvmPrParkeringsplass = 18.0;
            arealdisponering.antallParkeringsplasserTerreng = 4;


            // Act
            Utnyttingsgrad utnyttingsgrad = utnyttelsesgradController.PostProsentBRA(arealdisponering);

            // Assert
            Assert.NotNull(utnyttingsgrad);
            Assert.Equal(utnyttingsgrad.beregnetGradAvUtnytting, 7.0);
            Assert.Equal(utnyttingsgrad.statusKode, Utnyttingsgrad.UTNYTTINGSGRAD_SUCCESS);
            Assert.Equal(utnyttingsgrad.enhetForBeregning, Constants.returned_unit_percent);
            Assert.Contains(utnyttingsgrad.resultatBeskrivelse, Constants.resultatBeskrivelsePctBRA);

        }



        [Fact]
        public void Post_pctBRA_zeroTomt()
        {
            // Arrange
            ArealdisponeringMedTomt arealdisponering = new ArealdisponeringMedTomt();
            UtnyttelsesgradController utnyttelsesgradController = new UtnyttelsesgradController();

            // Populate input data for normal calculations
            arealdisponering.tomtearealByggeomraade = 0.0;
            arealdisponering.arealBebyggelseEksisterende = 200.56;
            arealdisponering.arealBebyggelseNytt = 50.66;
            arealdisponering.arealBebyggelseSomSkalRives = 22.5;
            arealdisponering.kravKvmPrParkeringsplass = 18.0;
            arealdisponering.antallParkeringsplasserTerreng = 4;


            // Act
            Utnyttingsgrad utnyttingsgrad = utnyttelsesgradController.PostProsentBRA(arealdisponering);

            // Assert
            Assert.NotNull(utnyttingsgrad);
            Assert.Equal(utnyttingsgrad.statusKode, Utnyttingsgrad.UTNYTTINGSGRAD_ERROR);
        }



        [Fact]
        public void Post_pctBRA_negativeInputParamter()
        {
            // Arrange
            ArealdisponeringMedTomt arealdisponering = new ArealdisponeringMedTomt();
            UtnyttelsesgradController utnyttelsesgradController = new UtnyttelsesgradController();

            // Populate input data for normal calculations
            arealdisponering.tomtearealByggeomraade = 3000.0;
            arealdisponering.arealBebyggelseEksisterende = -200.56;
            arealdisponering.arealBebyggelseNytt = 50.66;
            arealdisponering.arealBebyggelseSomSkalRives = 22.5;
            arealdisponering.kravKvmPrParkeringsplass = 18.0;
            arealdisponering.antallParkeringsplasserTerreng = 4;


            // Act
            Utnyttingsgrad utnyttingsgrad = utnyttelsesgradController.PostProsentBRA(arealdisponering);

            // Assert
            Assert.NotNull(utnyttingsgrad);
            Assert.Equal(utnyttingsgrad.statusKode, Utnyttingsgrad.UTNYTTINGSGRAD_ERROR);
        }



        [Fact]
        public void Post_pctBRA_negativeResult()
        {
            // Arrange
            ArealdisponeringMedTomt arealdisponering = new ArealdisponeringMedTomt();
            UtnyttelsesgradController utnyttelsesgradController = new UtnyttelsesgradController();

            // Populate input data for normal calculations
            arealdisponering.tomtearealByggeomraade = 3000.0;
            arealdisponering.arealBebyggelseEksisterende = 200.56;
            arealdisponering.arealBebyggelseNytt = 50.66;
            arealdisponering.arealBebyggelseSomSkalRives = 400.5;
            arealdisponering.kravKvmPrParkeringsplass = 18.0;
            arealdisponering.antallParkeringsplasserTerreng = 4;


            // Act
            Utnyttingsgrad utnyttingsgrad = utnyttelsesgradController.PostProsentBRA(arealdisponering);

            // Assert
            Assert.NotNull(utnyttingsgrad);
            Assert.Equal(utnyttingsgrad.statusKode, Utnyttingsgrad.UTNYTTINGSGRAD_ERROR);
        }



        // =============== TESTS for api/Utnyttelsesgrad/ProsentTU  ===============
        [Fact]
        public void Post_pctTU_normal()
        {
            // Arrange
            ArealdisponeringMedTomt arealdisponering = new ArealdisponeringMedTomt();
            UtnyttelsesgradController utnyttelsesgradController = new UtnyttelsesgradController();

            // Populate input data for normal calculations
            arealdisponering.tomtearealByggeomraade = 5000.34;
            arealdisponering.arealBebyggelseEksisterende = 200.56;
            arealdisponering.arealBebyggelseNytt = 50.66;
            arealdisponering.arealBebyggelseSomSkalRives = 22.5;
            arealdisponering.kravKvmPrParkeringsplass = 18.0;
            arealdisponering.antallParkeringsplasserTerreng = 4;


            // Act
            Utnyttingsgrad utnyttingsgrad = utnyttelsesgradController.PostProsentTU(arealdisponering);

            // Assert
            Assert.NotNull(utnyttingsgrad);
            Assert.Equal(utnyttingsgrad.beregnetGradAvUtnytting, 7.0);
            Assert.Equal(utnyttingsgrad.statusKode, Utnyttingsgrad.UTNYTTINGSGRAD_SUCCESS);
            Assert.Equal(utnyttingsgrad.enhetForBeregning, Constants.returned_unit_percent);
            Assert.Contains(utnyttingsgrad.resultatBeskrivelse, Constants.resultatBeskrivelsePctTU);

        }



        [Fact]
        public void Post_pctTU_zeroTomt()
        {
            // Arrange
            ArealdisponeringMedTomt arealdisponering = new ArealdisponeringMedTomt();
            UtnyttelsesgradController utnyttelsesgradController = new UtnyttelsesgradController();

            // Populate input data for normal calculations
            arealdisponering.tomtearealByggeomraade = 0.0;
            arealdisponering.arealBebyggelseEksisterende = 200.56;
            arealdisponering.arealBebyggelseNytt = 50.66;
            arealdisponering.arealBebyggelseSomSkalRives = 22.5;
            arealdisponering.kravKvmPrParkeringsplass = 18.0;
            arealdisponering.antallParkeringsplasserTerreng = 4;


            // Act
            Utnyttingsgrad utnyttingsgrad = utnyttelsesgradController.PostProsentTU(arealdisponering);

            // Assert
            Assert.NotNull(utnyttingsgrad);
            Assert.Equal(utnyttingsgrad.statusKode, Utnyttingsgrad.UTNYTTINGSGRAD_ERROR);
        }



        [Fact]
        public void Post_pctTU_negativeInputParamter()
        {
            // Arrange
            ArealdisponeringMedTomt arealdisponering = new ArealdisponeringMedTomt();
            UtnyttelsesgradController utnyttelsesgradController = new UtnyttelsesgradController();

            // Populate input data for normal calculations
            arealdisponering.tomtearealByggeomraade = 3000.0;
            arealdisponering.arealBebyggelseEksisterende = -200.56;
            arealdisponering.arealBebyggelseNytt = 50.66;
            arealdisponering.arealBebyggelseSomSkalRives = 22.5;
            arealdisponering.kravKvmPrParkeringsplass = 18.0;
            arealdisponering.antallParkeringsplasserTerreng = 4;


            // Act
            Utnyttingsgrad utnyttingsgrad = utnyttelsesgradController.PostProsentTU(arealdisponering);

            // Assert
            Assert.NotNull(utnyttingsgrad);
            Assert.Equal(utnyttingsgrad.statusKode, Utnyttingsgrad.UTNYTTINGSGRAD_ERROR);
        }



        [Fact]
        public void Post_pctTU_negativeResult()
        {
            // Arrange
            ArealdisponeringMedTomt arealdisponering = new ArealdisponeringMedTomt();
            UtnyttelsesgradController utnyttelsesgradController = new UtnyttelsesgradController();

            // Populate input data for normal calculations
            arealdisponering.tomtearealByggeomraade = 3000.0;
            arealdisponering.arealBebyggelseEksisterende = 200.56;
            arealdisponering.arealBebyggelseNytt = 50.66;
            arealdisponering.arealBebyggelseSomSkalRives = 400.5;
            arealdisponering.kravKvmPrParkeringsplass = 18.0;
            arealdisponering.antallParkeringsplasserTerreng = 4;


            // Act
            Utnyttingsgrad utnyttingsgrad = utnyttelsesgradController.PostProsentTU(arealdisponering);

            // Assert
            Assert.NotNull(utnyttingsgrad);
            Assert.Equal(utnyttingsgrad.statusKode, Utnyttingsgrad.UTNYTTINGSGRAD_ERROR);
        }



        // =============== TESTS for api/Utnyttelsesgrad/TillattBebygdAreal  ===============
        [Fact]
        public void Post_TillattBebygdAreal_normal()
        {
            // Arrange
            ArealdisponeringMedTomt arealdisponering = new ArealdisponeringMedTomt();
            UtnyttelsesgradController utnyttelsesgradController = new UtnyttelsesgradController();

            // Populate input data for normal calculations
            arealdisponering.tomtearealByggeomraade = 300.9;
            arealdisponering.arealBebyggelseEksisterende = 123.56;
            arealdisponering.arealBebyggelseNytt = 23.66;
            arealdisponering.arealBebyggelseSomSkalRives = 12.5;
            arealdisponering.kravKvmPrParkeringsplass = 18.0;
            arealdisponering.antallParkeringsplasserTerreng = 2;


            // Act
            Utnyttingsgrad utnyttingsgrad = utnyttelsesgradController.PostTillattBebygdAreal(arealdisponering);

            // Assert
            Assert.NotNull(utnyttingsgrad);
            Assert.Equal(utnyttingsgrad.statusKode, Utnyttingsgrad.UTNYTTINGSGRAD_SUCCESS);
            Assert.Equal(utnyttingsgrad.beregnetGradAvUtnytting, 56.7, 1); // 1 decimal precision
            Assert.Equal(utnyttingsgrad.enhetForBeregning, Constants.returned_unit_percent);
            Assert.Contains(utnyttingsgrad.resultatBeskrivelse, Constants.resultatBeskrivelseTillattBebygdAreal);
        }



        [Fact]
        public void Post_TillattBebygdAreal_zeroTomt()
        {
            // Arrange
            ArealdisponeringMedTomt arealdisponering = new ArealdisponeringMedTomt();
            UtnyttelsesgradController utnyttelsesgradController = new UtnyttelsesgradController();

            // Populate input data for normal calculations
            arealdisponering.tomtearealByggeomraade = 0.0;
            arealdisponering.arealBebyggelseEksisterende = 123.56;
            arealdisponering.arealBebyggelseNytt = 23.66;
            arealdisponering.arealBebyggelseSomSkalRives = 12.5;
            arealdisponering.kravKvmPrParkeringsplass = 18.0;
            arealdisponering.antallParkeringsplasserTerreng = 2;


            // Act
            Utnyttingsgrad utnyttingsgrad = utnyttelsesgradController.PostTillattBebygdAreal(arealdisponering);

            // Assert
            Assert.NotNull(utnyttingsgrad);
            Assert.Equal(utnyttingsgrad.statusKode, Utnyttingsgrad.UTNYTTINGSGRAD_ERROR);
        }



        [Fact]
        public void Post_TillattBebygdAreal_negativeInput()
        {
            // Arrange
            ArealdisponeringMedTomt arealdisponering = new ArealdisponeringMedTomt();
            UtnyttelsesgradController utnyttelsesgradController = new UtnyttelsesgradController();

            // Populate input data for normal calculations
            arealdisponering.tomtearealByggeomraade = 200.0;
            arealdisponering.arealBebyggelseEksisterende = 123.56;
            arealdisponering.arealBebyggelseNytt = -23.66;
            arealdisponering.arealBebyggelseSomSkalRives = 12.5;
            arealdisponering.kravKvmPrParkeringsplass = 18.0;
            arealdisponering.antallParkeringsplasserTerreng = 2;


            // Act
            Utnyttingsgrad utnyttingsgrad = utnyttelsesgradController.PostTillattBebygdAreal(arealdisponering);

            // Assert
            Assert.NotNull(utnyttingsgrad);
            Assert.Equal(utnyttingsgrad.statusKode, Utnyttingsgrad.UTNYTTINGSGRAD_ERROR);
        }


        [Fact]
        public void Post_TillattBebygdAreal_negativeResult()
        {
            // Arrange
            ArealdisponeringMedTomt arealdisponering = new ArealdisponeringMedTomt();
            UtnyttelsesgradController utnyttelsesgradController = new UtnyttelsesgradController();

            // Populate input data for normal calculations
            arealdisponering.tomtearealByggeomraade = 200.0;
            arealdisponering.arealBebyggelseEksisterende = 123.56;
            arealdisponering.arealBebyggelseNytt = 23.66;
            arealdisponering.arealBebyggelseSomSkalRives = 2000.5;
            arealdisponering.kravKvmPrParkeringsplass = 18.0;
            arealdisponering.antallParkeringsplasserTerreng = 2;


            // Act
            Utnyttingsgrad utnyttingsgrad = utnyttelsesgradController.PostTillattBebygdAreal(arealdisponering);

            // Assert
            Assert.NotNull(utnyttingsgrad);
            Assert.Equal(utnyttingsgrad.statusKode, Utnyttingsgrad.UTNYTTINGSGRAD_ERROR);
        }



        // =============== TESTS for api/Utnyttelsesgrad/TillattTomteUtnyttelse  ===============
        [Fact]
        public void Post_TillattTomteUtnyttelse_normal()
        {
            // Arrange
            ArealdisponeringMedTomt arealdisponering = new ArealdisponeringMedTomt();
            UtnyttelsesgradController utnyttelsesgradController = new UtnyttelsesgradController();

            // Populate input data for normal calculations
            arealdisponering.tomtearealByggeomraade = 300.9;
            arealdisponering.arealBebyggelseEksisterende = 123.56;
            arealdisponering.arealBebyggelseNytt = 23.66;
            arealdisponering.arealBebyggelseSomSkalRives = 12.5;
            arealdisponering.kravKvmPrParkeringsplass = 18.0;
            arealdisponering.antallParkeringsplasserTerreng = 2;


            // Act
            Utnyttingsgrad utnyttingsgrad = utnyttelsesgradController.PostTillattTomteUtnyttelse(arealdisponering);

            // Assert
            Assert.NotNull(utnyttingsgrad);
            Assert.Equal(utnyttingsgrad.statusKode, Utnyttingsgrad.UTNYTTINGSGRAD_SUCCESS);
            Assert.Equal(utnyttingsgrad.beregnetGradAvUtnytting, 56.7, 1); // 1 decimal precision
            Assert.Equal(utnyttingsgrad.enhetForBeregning, Constants.returned_unit_percent);
            Assert.Contains(utnyttingsgrad.resultatBeskrivelse, Constants.resultatBeskrivelseTillattTomteutnyttelse);
        }



        [Fact]
        public void Post_TillattTomteUtnyttelse_zeroTomt()
        {
            // Arrange
            ArealdisponeringMedTomt arealdisponering = new ArealdisponeringMedTomt();
            UtnyttelsesgradController utnyttelsesgradController = new UtnyttelsesgradController();

            // Populate input data for normal calculations
            arealdisponering.tomtearealByggeomraade = 0.0;
            arealdisponering.arealBebyggelseEksisterende = 123.56;
            arealdisponering.arealBebyggelseNytt = 23.66;
            arealdisponering.arealBebyggelseSomSkalRives = 12.5;
            arealdisponering.kravKvmPrParkeringsplass = 18.0;
            arealdisponering.antallParkeringsplasserTerreng = 2;


            // Act
            Utnyttingsgrad utnyttingsgrad = utnyttelsesgradController.PostTillattTomteUtnyttelse(arealdisponering);

            // Assert
            Assert.NotNull(utnyttingsgrad);
            Assert.Equal(utnyttingsgrad.statusKode, Utnyttingsgrad.UTNYTTINGSGRAD_ERROR);
        }



        [Fact]
        public void Post_TillattTomteUtnyttelse_negativeInput()
        {
            // Arrange
            ArealdisponeringMedTomt arealdisponering = new ArealdisponeringMedTomt();
            UtnyttelsesgradController utnyttelsesgradController = new UtnyttelsesgradController();

            // Populate input data for normal calculations
            arealdisponering.tomtearealByggeomraade = 200.0;
            arealdisponering.arealBebyggelseEksisterende = 123.56;
            arealdisponering.arealBebyggelseNytt = -23.66;
            arealdisponering.arealBebyggelseSomSkalRives = 12.5;
            arealdisponering.kravKvmPrParkeringsplass = 18.0;
            arealdisponering.antallParkeringsplasserTerreng = 2;


            // Act
            Utnyttingsgrad utnyttingsgrad = utnyttelsesgradController.PostTillattTomteUtnyttelse(arealdisponering);

            // Assert
            Assert.NotNull(utnyttingsgrad);
            Assert.Equal(utnyttingsgrad.statusKode, Utnyttingsgrad.UTNYTTINGSGRAD_ERROR);
        }


        [Fact]
        public void Post_TillattTomteUtnyttelse_negativeResult()
        {
            // Arrange
            ArealdisponeringMedTomt arealdisponering = new ArealdisponeringMedTomt();
            UtnyttelsesgradController utnyttelsesgradController = new UtnyttelsesgradController();

            // Populate input data for normal calculations
            arealdisponering.tomtearealByggeomraade = 200.0;
            arealdisponering.arealBebyggelseEksisterende = 123.56;
            arealdisponering.arealBebyggelseNytt = 23.66;
            arealdisponering.arealBebyggelseSomSkalRives = 2000.5;
            arealdisponering.kravKvmPrParkeringsplass = 18.0;
            arealdisponering.antallParkeringsplasserTerreng = 2;


            // Act
            Utnyttingsgrad utnyttingsgrad = utnyttelsesgradController.PostTillattTomteUtnyttelse(arealdisponering);

            // Assert
            Assert.NotNull(utnyttingsgrad);
            Assert.Equal(utnyttingsgrad.statusKode, Utnyttingsgrad.UTNYTTINGSGRAD_ERROR);
        }



        // =============== TESTS for api/Utnyttelsesgrad/TillattBruksareal  ===============
        [Fact]
        public void Post_TillattBruksareal_normal()
        {
            // Arrange
            ArealdisponeringUtenTomt arealdisponering = new ArealdisponeringUtenTomt();
            UtnyttelsesgradController utnyttelsesgradController = new UtnyttelsesgradController();

            // Populate input data for normal calculations
            arealdisponering.arealBebyggelseEksisterende = 200.5;
            arealdisponering.arealBebyggelseNytt = 75.3;
            arealdisponering.arealBebyggelseSomSkalRives = 10.0;
            arealdisponering.kravKvmPrParkeringsplass = 18.0;
            arealdisponering.antallParkeringsplasserTerreng = 2;


            // Act
            Utnyttingsgrad utnyttingsgrad = utnyttelsesgradController.PostTillattBruksareal(arealdisponering);

            // Assert
            Assert.NotNull(utnyttingsgrad);
            Assert.Equal(utnyttingsgrad.beregnetGradAvUtnytting, 301.8,1);
            Assert.Equal(utnyttingsgrad.statusKode, Utnyttingsgrad.UTNYTTINGSGRAD_SUCCESS);
            Assert.Equal(utnyttingsgrad.enhetForBeregning, Constants.returned_unit_m2);
            Assert.Contains(utnyttingsgrad.resultatBeskrivelse, Constants.resultatBeskrivelseTillattBruksareal);

        }



        [Fact]
        public void Post_TillattBruksareal_negativeValues()
        {
            // Arrange
            ArealdisponeringUtenTomt arealdisponering = new ArealdisponeringUtenTomt();
            UtnyttelsesgradController utnyttelsesgradController = new UtnyttelsesgradController();

            // Populate input data for normal calculations
            arealdisponering.arealBebyggelseEksisterende = 123.56;
            arealdisponering.arealBebyggelseNytt = 23.66;
            arealdisponering.arealBebyggelseSomSkalRives = -12.5;
            arealdisponering.kravKvmPrParkeringsplass = 18.0;
            arealdisponering.antallParkeringsplasserTerreng = 2;


            // Act
            Utnyttingsgrad utnyttingsgrad = utnyttelsesgradController.PostTillattBruksareal(arealdisponering);

            // Assert
            Assert.NotNull(utnyttingsgrad);
            Assert.Equal(utnyttingsgrad.statusKode, Utnyttingsgrad.UTNYTTINGSGRAD_ERROR);
        }



        [Fact]
        public void Post_TillattBruksareal_negativeResult()
        {
            // Arrange
            ArealdisponeringUtenTomt arealdisponering = new ArealdisponeringUtenTomt();
            UtnyttelsesgradController utnyttelsesgradController = new UtnyttelsesgradController();

            // Populate input data for normal calculations
            arealdisponering.arealBebyggelseEksisterende = 123.56;
            arealdisponering.arealBebyggelseNytt = 23.66;
            arealdisponering.arealBebyggelseSomSkalRives = 300.5;
            arealdisponering.kravKvmPrParkeringsplass = 18.0;
            arealdisponering.antallParkeringsplasserTerreng = 2;


            // Act
            Utnyttingsgrad utnyttingsgrad = utnyttelsesgradController.PostTillattBruksareal(arealdisponering);

            // Assert
            Assert.NotNull(utnyttingsgrad);
            Assert.Equal(utnyttingsgrad.statusKode, Utnyttingsgrad.UTNYTTINGSGRAD_ERROR);
        }



        // =============== TESTS for api/Utnyttelsesgrad/TillattBruksarealTBRA  ===============
        [Fact]
        public void Post_TillattBruksarealTBRA_normal()
        {
            // Arrange
            ArealdisponeringUtenTomt arealdisponering = new ArealdisponeringUtenTomt();
            UtnyttelsesgradController utnyttelsesgradController = new UtnyttelsesgradController();

            // Populate input data for normal calculations
            arealdisponering.arealBebyggelseEksisterende = 200.5;
            arealdisponering.arealBebyggelseNytt = 75.3;
            arealdisponering.arealBebyggelseSomSkalRives = 10.0;
            arealdisponering.kravKvmPrParkeringsplass = 18.0;
            arealdisponering.antallParkeringsplasserTerreng = 2;


            // Act
            Utnyttingsgrad utnyttingsgrad = utnyttelsesgradController.PostTillattBruksarealTBRA(arealdisponering);

            // Assert
            Assert.NotNull(utnyttingsgrad);
            Assert.Equal(utnyttingsgrad.beregnetGradAvUtnytting, 302.0);
            Assert.Equal(utnyttingsgrad.statusKode, Utnyttingsgrad.UTNYTTINGSGRAD_SUCCESS);
            Assert.Equal(utnyttingsgrad.enhetForBeregning, Constants.returned_unit_m2);
            Assert.Contains(utnyttingsgrad.resultatBeskrivelse, Constants.resultatBeskrivelseTBRA);

        }



        [Fact]
        public void Post_TillattBruksarealTBRA_negativeValues()
        {
            // Arrange
            ArealdisponeringUtenTomt arealdisponering = new ArealdisponeringUtenTomt();
            UtnyttelsesgradController utnyttelsesgradController = new UtnyttelsesgradController();

            // Populate input data for normal calculations
            arealdisponering.arealBebyggelseEksisterende = 123.56;
            arealdisponering.arealBebyggelseNytt = 23.66;
            arealdisponering.arealBebyggelseSomSkalRives = -12.5;
            arealdisponering.kravKvmPrParkeringsplass = 18.0;
            arealdisponering.antallParkeringsplasserTerreng = 2;


            // Act
            Utnyttingsgrad utnyttingsgrad = utnyttelsesgradController.PostTillattBruksarealTBRA(arealdisponering);

            // Assert
            Assert.NotNull(utnyttingsgrad);
            Assert.Equal(utnyttingsgrad.statusKode, Utnyttingsgrad.UTNYTTINGSGRAD_ERROR);
        }



        [Fact]
        public void Post_TillattBruksarealTBRA_negativeResult()
        {
            // Arrange
            ArealdisponeringUtenTomt arealdisponering = new ArealdisponeringUtenTomt();
            UtnyttelsesgradController utnyttelsesgradController = new UtnyttelsesgradController();

            // Populate input data for normal calculations
            arealdisponering.arealBebyggelseEksisterende = 123.56;
            arealdisponering.arealBebyggelseNytt = 23.66;
            arealdisponering.arealBebyggelseSomSkalRives = 300.5;
            arealdisponering.kravKvmPrParkeringsplass = 18.0;
            arealdisponering.antallParkeringsplasserTerreng = 2;


            // Act
            Utnyttingsgrad utnyttingsgrad = utnyttelsesgradController.PostTillattBruksarealTBRA(arealdisponering);

            // Assert
            Assert.NotNull(utnyttingsgrad);
            Assert.Equal(utnyttingsgrad.statusKode, Utnyttingsgrad.UTNYTTINGSGRAD_ERROR);
        }


        // =============== TESTS for api/Utnyttelsesgrad/GolvarealDeltPaaGrunnareal   ===============
        [Fact]
        public void Post_GolvarealDeltPaaGrunnareal_normal()
        {
            // Arrange
            ArealDisponeringMedTomtUtenParkering arealdisponering = new ArealDisponeringMedTomtUtenParkering();
            UtnyttelsesgradController utnyttelsesgradController = new UtnyttelsesgradController();

            // Populate input data for normal calculations
            arealdisponering.tomtearealByggeomraade = 400.45;
            arealdisponering.arealBebyggelseEksisterende = 100.5;
            arealdisponering.arealBebyggelseNytt = 20.3;
            arealdisponering.arealBebyggelseSomSkalRives = 5.0;


            // Act
            Utnyttingsgrad utnyttingsgrad = utnyttelsesgradController.PostGolvarealDeltPaaGrunnareal(arealdisponering);

            // Assert
            Assert.NotNull(utnyttingsgrad);
            Assert.Equal(utnyttingsgrad.beregnetGradAvUtnytting, 0.289, 3);
            Assert.Equal(utnyttingsgrad.statusKode, Utnyttingsgrad.UTNYTTINGSGRAD_SUCCESS);
            Assert.Equal(utnyttingsgrad.enhetForBeregning, Constants.returned_unit_ratio);
            Assert.Contains(utnyttingsgrad.resultatBeskrivelse, Constants.resultatBeskrivelseBruttoGolvOverGrunn);

        }




        public void Post_GolvarealDeltPaaGrunnareal_zeroTomt()
        {
            // Arrange
            ArealDisponeringMedTomtUtenParkering arealdisponering = new ArealDisponeringMedTomtUtenParkering();
            UtnyttelsesgradController utnyttelsesgradController = new UtnyttelsesgradController();

            // Populate input data for normal calculations
            arealdisponering.tomtearealByggeomraade = 0.0;
            arealdisponering.arealBebyggelseEksisterende = 100.5;
            arealdisponering.arealBebyggelseNytt = 20.3;
            arealdisponering.arealBebyggelseSomSkalRives = 5.0;


            // Act
            Utnyttingsgrad utnyttingsgrad = utnyttelsesgradController.PostGolvarealDeltPaaGrunnareal(arealdisponering);

            // Assert
            Assert.NotNull(utnyttingsgrad);
            Assert.Equal(utnyttingsgrad.statusKode, Utnyttingsgrad.UTNYTTINGSGRAD_ERROR);

        }



        public void Post_GolvarealDeltPaaGrunnareal_negativeInput()
        {
            // Arrange
            ArealDisponeringMedTomtUtenParkering arealdisponering = new ArealDisponeringMedTomtUtenParkering();
            UtnyttelsesgradController utnyttelsesgradController = new UtnyttelsesgradController();

            // Populate input data for normal calculations
            arealdisponering.tomtearealByggeomraade = 400.0;
            arealdisponering.arealBebyggelseEksisterende = -100.5;
            arealdisponering.arealBebyggelseNytt = 20.3;
            arealdisponering.arealBebyggelseSomSkalRives = 5.0;


            // Act
            Utnyttingsgrad utnyttingsgrad = utnyttelsesgradController.PostGolvarealDeltPaaGrunnareal(arealdisponering);

            // Assert
            Assert.NotNull(utnyttingsgrad);
            Assert.Equal(utnyttingsgrad.statusKode, Utnyttingsgrad.UTNYTTINGSGRAD_ERROR);

        }


        public void Post_GolvarealDeltPaaGrunnareal_negativeResult()
        {
            // Arrange
            ArealDisponeringMedTomtUtenParkering arealdisponering = new ArealDisponeringMedTomtUtenParkering();
            UtnyttelsesgradController utnyttelsesgradController = new UtnyttelsesgradController();

            // Populate input data for normal calculations
            arealdisponering.tomtearealByggeomraade = 400.0;
            arealdisponering.arealBebyggelseEksisterende = 100.5;
            arealdisponering.arealBebyggelseNytt = 20.3;
            arealdisponering.arealBebyggelseSomSkalRives = 600.0;


            // Act
            Utnyttingsgrad utnyttingsgrad = utnyttelsesgradController.PostGolvarealDeltPaaGrunnareal(arealdisponering);

            // Assert
            Assert.NotNull(utnyttingsgrad);
            Assert.Equal(utnyttingsgrad.statusKode, Utnyttingsgrad.UTNYTTINGSGRAD_ERROR);

        }





    }
}
