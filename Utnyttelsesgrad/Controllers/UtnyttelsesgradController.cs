﻿using DIBK.FellestjenesterBygg.Utnyttelsesgrad.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Serilog;
using DIBK.FellestjenesterBygg.Utnyttelsesgrad.Services;


namespace DIBK.FellestjenesterBygg.Utnyttelsesgrad.Controllers
{
    /// <summary>
    /// Utnyttelsesgrad API funksjoner
    /// </summary>
    public class UtnyttelsesgradController : ApiController
    {

        //------------------------------------------------------------------------------------------------------------------------------------
        //     PostBYA
        //------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        ///  Utnyttelsesgrad beregnet med metode Bebygd areal, BYA, i m&#178; avrundet opp til helt tall 
        /// </summary>
        /// <param name="arealdisponeringUtenTomt"></param>
        /// <returns></returns>
        // POST api/Utnyttelsesgrad/BYA
        [ActionName("BYA")]
        public Utnyttingsgrad PostBYA([FromBody] ArealdisponeringUtenTomt arealdisponeringUtenTomt)
        {

            string url = "api/Utnyttelsesgrad/BYA";

            UtnyttelsesgradRequestProcessing utnyttelsesgradRequestProcessing = new UtnyttelsesgradRequestProcessing();
            Utnyttingsgrad utnyttingsgrad = new Utnyttingsgrad();

            Log.Information("*** {url} called with these parameters:", url);
            Log.Information("{@arealdisponeringUtenTomt}", arealdisponeringUtenTomt);

            utnyttingsgrad = utnyttelsesgradRequestProcessing.areaCalculation(arealdisponeringUtenTomt, Constants.METHOD_BEBYGDAREAL_BYA);

            if (utnyttingsgrad.statusKode == Utnyttingsgrad.UTNYTTINGSGRAD_SUCCESS)
            {
                utnyttingsgrad.resultatBeskrivelse = Constants.resultatBeskrivelseBYA;
                Log.Information("{url} calculation completed with this result:", url);
            }
            else
            {
                Log.Information("{url} could not be completed ... returning an error:", url);
            }

            // Log the result variable
            Log.Information("{@utnyttingsgrad}", utnyttingsgrad);
            return utnyttingsgrad;

        }





        //------------------------------------------------------------------------------------------------------------------------------------
        //     PostProsentBYA
        //------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Utnyttelsesgrad beregnet med metode Prosent bebygd areal, %-BYA, i prosent avrundet opp til helt tall 
        /// </summary>
        /// <param name="arealdisponeringMedTomt"></param>
        /// <returns></returns>
        // POST api/Utnyttelsesgrad/ProsentBYA
        [ActionName("ProsentBYA")]
        public Utnyttingsgrad PostProsentBYA([FromBody] ArealdisponeringMedTomt arealdisponeringMedTomt)
        {

            string url = "api/Utnyttelsesgrad/ProsentBYA";

            UtnyttelsesgradRequestProcessing utnyttelsesgradRequestProcessing = new UtnyttelsesgradRequestProcessing();
            Utnyttingsgrad utnyttingsgrad = new Utnyttingsgrad();

            Log.Information("*** {url} called with these parameters:", url);
            Log.Information("{@arealdisponeringMedTomt}", arealdisponeringMedTomt);

            utnyttingsgrad =  utnyttelsesgradRequestProcessing.percentageCalculation(arealdisponeringMedTomt, Constants.METHOD_PROSENTBEBYGDAREAL_pctBYA);

            if (utnyttingsgrad.statusKode == Utnyttingsgrad.UTNYTTINGSGRAD_SUCCESS)
            {
                utnyttingsgrad.resultatBeskrivelse = Constants.resultatBeskrivelsePctBYA;
                Log.Information("{url} calculation completed with this result:", url);
            }
            else
            {
                Log.Information("{url} could not be completed ... returning an error:", url);
            }

            // Log the result variable
            Log.Information("{@utnyttingsgrad}", utnyttingsgrad);
            return utnyttingsgrad;
        }






        //------------------------------------------------------------------------------------------------------------------------------------
        //     PostBRA
        //------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        ///  Utnyttelsesgrad beregnet med metode Bruksareal, BRA, i m&#178; avrundet opp til helt tall  
        /// </summary>
        /// <param name="arealdisponeringUtenTomt"></param>
        /// <returns></returns>
        // POST api/Utnyttelsesgrad/BRA
        [ActionName("BRA")]
        public Utnyttingsgrad PostBRA([FromBody] ArealdisponeringUtenTomt arealdisponeringUtenTomt)
        {

            string url = "api/Utnyttelsesgrad/BRA";

            UtnyttelsesgradRequestProcessing utnyttelsesgradRequestProcessing = new UtnyttelsesgradRequestProcessing();
            Utnyttingsgrad utnyttingsgrad = new Utnyttingsgrad();

            Log.Information("*** {url} called with these parameters:", url);
            Log.Information("{@arealdisponeringUtenTomt}", arealdisponeringUtenTomt);

            utnyttingsgrad = utnyttelsesgradRequestProcessing.areaCalculation(arealdisponeringUtenTomt, Constants.METHOD_BRUKSAREAL_BRA);

            if (utnyttingsgrad.statusKode == Utnyttingsgrad.UTNYTTINGSGRAD_SUCCESS)
            {
                utnyttingsgrad.resultatBeskrivelse = Constants.resultatBeskrivelseBRA;
                Log.Information("{url} calculation completed with this result:", url);
            }
            else
            {
                Log.Information("{url} could not be completed ... returning an error:", url);
            }

            // Log the result variable
            Log.Information("{@utnyttingsgrad}", utnyttingsgrad);
            return utnyttingsgrad;
        }





        //------------------------------------------------------------------------------------------------------------------------------------
        //     PostProsentBRA
        //------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Utnyttelsesgrad beregnet med metode Prosent bruksareal, %-BRA, i prosent avrundet opp til helt tall  
        /// </summary>
        /// <param name="arealdisponeringMedTomt"></param>
        /// <returns></returns>
        // POST api/Utnyttelsesgrad/ProsentBRA
        [ActionName("ProsentBRA")]
        public Utnyttingsgrad PostProsentBRA([FromBody] ArealdisponeringMedTomt arealdisponeringMedTomt)
        {

            string url = "api/Utnyttelsesgrad/ProsentBRA";

            UtnyttelsesgradRequestProcessing utnyttelsesgradRequestProcessing = new UtnyttelsesgradRequestProcessing();
            Utnyttingsgrad utnyttingsgrad = new Utnyttingsgrad();

            Log.Information("*** {url} called with these parameters:", url);
            Log.Information("{@arealdisponeringMedTomt}", arealdisponeringMedTomt);

            utnyttingsgrad = utnyttelsesgradRequestProcessing.percentageCalculation(arealdisponeringMedTomt, Constants.METHOD_PROSENTBRUKSAREAL_pctBRA);

            if (utnyttingsgrad.statusKode == Utnyttingsgrad.UTNYTTINGSGRAD_SUCCESS)
            {
                utnyttingsgrad.resultatBeskrivelse = Constants.resultatBeskrivelsePctBRA;
                Log.Information("{url} calculation completed with this result:", url);
            }
            else
            {
                Log.Information("{url} could not be completed ... returning an error:", url);
            }

            // Log the result variable
            Log.Information("{@utnyttingsgrad}", utnyttingsgrad);
            return utnyttingsgrad;
        }





        //------------------------------------------------------------------------------------------------------------------------------------
        //     PostProsentTU
        //------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Utnyttelsesgrad beregnet med metode Tomteutnyttelse, %TU, i prosent avrundet opp til helt tall  
        /// </summary>
        /// <param name="arealdisponeringMedTomt"></param>
        /// <returns></returns>
        // POST api/Utnyttelsesgrad/ProsentTU
        [ActionName("ProsentTU")]
        public Utnyttingsgrad PostProsentTU([FromBody] ArealdisponeringMedTomt arealdisponeringMedTomt)
        {

            string url = "api/Utnyttelsesgrad/ProsentTU";

            UtnyttelsesgradRequestProcessing utnyttelsesgradRequestProcessing = new UtnyttelsesgradRequestProcessing();
            Utnyttingsgrad utnyttingsgrad = new Utnyttingsgrad();

            Log.Information("*** {url} called with these parameters:", url);
            Log.Information("{@arealdisponeringMedTomt}", arealdisponeringMedTomt);

            utnyttingsgrad = utnyttelsesgradRequestProcessing.percentageCalculation(arealdisponeringMedTomt, Constants.METHOD_PROSENTTOMTEUTNYTTELSE_pctBRA);

            if (utnyttingsgrad.statusKode == Utnyttingsgrad.UTNYTTINGSGRAD_SUCCESS)
            {
                utnyttingsgrad.resultatBeskrivelse = Constants.resultatBeskrivelsePctTU;
                Log.Information("{url} calculation completed with this result:", url);
            }
            else
            {
                Log.Information("{url} could not be completed ... returning an error:", url);
            }

            // Log the result variable
            Log.Information("{@utnyttingsgrad}", utnyttingsgrad);
            return utnyttingsgrad;
        }





        //------------------------------------------------------------------------------------------------------------------------------------
        //     PostTillattBebygdAreal
        //------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Utnyttelsesgrad beregnet med metode tillatt bebygd areal,  Reguleringsplan 1987 - 1997, i prosent av tomtens areal, ikke avrundet  
        /// </summary>
        /// <param name="arealdisponeringMedTomt"></param>
        /// <returns></returns>
        // POST api/Utnyttelsesgrad/TillattBebygdAreal
        [ActionName("TillattBebygdAreal")]
        public Utnyttingsgrad PostTillattBebygdAreal([FromBody] ArealdisponeringMedTomt arealdisponeringMedTomt)
        {
            string url = "api/Utnyttelsesgrad/TillattBebygdAreal";

            UtnyttelsesgradRequestProcessing utnyttelsesgradRequestProcessing = new UtnyttelsesgradRequestProcessing();
            Utnyttingsgrad utnyttingsgrad = new Utnyttingsgrad();

            Log.Information("*** {url} called with these parameters:", url);
            Log.Information("{@arealdisponeringMedTomt}", arealdisponeringMedTomt);

            utnyttingsgrad = utnyttelsesgradRequestProcessing.percentageCalculation(arealdisponeringMedTomt, Constants.METHOD_TILLATT_BEBYGDAREAL);

            if (utnyttingsgrad.statusKode == Utnyttingsgrad.UTNYTTINGSGRAD_SUCCESS)
            {
                utnyttingsgrad.resultatBeskrivelse = Constants.resultatBeskrivelseTillattBebygdAreal;
                Log.Information("{url} calculation completed with this result:", url);
            }
            else
            {
                Log.Information("{url} could not be completed ... returning an error:", url);
            }

            // Log the result variable
            Log.Information("{@utnyttingsgrad}", utnyttingsgrad);
            return utnyttingsgrad;
        }





        //------------------------------------------------------------------------------------------------------------------------------------
        //     PostTillattBruksareal
        //------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Utnyttelsesgrad beregnet med metode tillatt bruksareal,  Reguleringsplan 1987 - 1997, Tillatt bruksareal for bebyggelse på en tomt, i m&#178;, ikke avrundet
        /// </summary>
        /// <param name="arealdisponeringUtenTomt"></param>
        /// <returns></returns>
        // POST api/Utnyttelsesgrad/TillattBruksareal
        [ActionName("TillattBruksareal")]
        public Utnyttingsgrad PostTillattBruksareal([FromBody] ArealdisponeringUtenTomt arealdisponeringUtenTomt)
        {
            string url = "api/Utnyttelsesgrad/TillattBruksareal";

            UtnyttelsesgradRequestProcessing utnyttelsesgradRequestProcessing = new UtnyttelsesgradRequestProcessing();
            Utnyttingsgrad utnyttingsgrad = new Utnyttingsgrad();

            Log.Information("*** {url} called with these parameters:", url);
            Log.Information("{@arealdisponeringUtenTomt}", arealdisponeringUtenTomt);

            utnyttingsgrad = utnyttelsesgradRequestProcessing.areaCalculation(arealdisponeringUtenTomt, Constants.METHOD_TILLATT_BRUKSAREAL);

            if (utnyttingsgrad.statusKode == Utnyttingsgrad.UTNYTTINGSGRAD_SUCCESS)
            {
                utnyttingsgrad.resultatBeskrivelse = Constants.resultatBeskrivelseTillattBruksareal;
                Log.Information("{url} calculation completed with this result:", url);
            }
            else
            {
                Log.Information("{url} could not be completed ... returning an error:", url);
            }

            // Log the result variable
            Log.Information("{@utnyttingsgrad}", utnyttingsgrad);
            return utnyttingsgrad;

        }



        //------------------------------------------------------------------------------------------------------------------------------------
        //     PostTillattBruksarealTBRA
        //------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Utnyttelsesgrad beregnet med metode tillatt bruksareal,  Reguleringsplan 1997 - 2007, Tillatt bruksareal for bebyggelse på en tomt, T-BRA, i m&#178; avrundet opp til helt tall  
        /// </summary>
        /// <param name="arealdisponeringUtenTomt"></param>
        /// <returns></returns>
        // POST api/Utnyttelsesgrad/TillattBruksareal
        [ActionName("TillattBruksarealTBRA")]
        public Utnyttingsgrad PostTillattBruksarealTBRA([FromBody] ArealdisponeringUtenTomt arealdisponeringUtenTomt)
        {
            string url = "api/Utnyttelsesgrad/TillattBruksarealTBRA";

            UtnyttelsesgradRequestProcessing utnyttelsesgradRequestProcessing = new UtnyttelsesgradRequestProcessing();
            Utnyttingsgrad utnyttingsgrad = new Utnyttingsgrad();

            Log.Information("*** {url} called with these parameters:", url);
            Log.Information("{@arealdisponeringUtenTomt}", arealdisponeringUtenTomt);

            utnyttingsgrad = utnyttelsesgradRequestProcessing.areaCalculation(arealdisponeringUtenTomt, Constants.METHOD_TILLATT_BRUKSAREAL_TBRA);

            if (utnyttingsgrad.statusKode == Utnyttingsgrad.UTNYTTINGSGRAD_SUCCESS)
            {
                utnyttingsgrad.resultatBeskrivelse = Constants.resultatBeskrivelseTBRA;
                Log.Information("{url} calculation completed with this result:", url);
            }
            else
            {
                Log.Information("{url} could not be completed ... returning an error:", url);
            }

            // Log the result variable
            Log.Information("{@utnyttingsgrad}", utnyttingsgrad);
            return utnyttingsgrad;

        }






        //------------------------------------------------------------------------------------------------------------------------------------
        //     PostTillattTomteUtnyttelse
        //------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Utnyttelsesgrad beregnet med metode tillatt tomteutnyttelse, Reguleringsplan 1987 - 1997, bruksareal, i prosent av tomtens areal, ikke avrundet  
        /// </summary>
        /// <param name="arealdisponeringMedTomt"></param>
        /// <returns></returns>
        // POST api/Utnyttelsesgrad/TillattTomteUtnyttelse
        [ActionName("TillattTomteUtnyttelse")]
        public Utnyttingsgrad PostTillattTomteUtnyttelse([FromBody] ArealdisponeringMedTomt arealdisponeringMedTomt)
        {
            string url = "api/Utnyttelsesgrad/TillattTomteUtnyttelse";

            UtnyttelsesgradRequestProcessing utnyttelsesgradRequestProcessing = new UtnyttelsesgradRequestProcessing();
            Utnyttingsgrad utnyttingsgrad = new Utnyttingsgrad();

            Log.Information("*** {url} called with these parameters:", url);
            Log.Information("{@arealdisponeringMedTomt}", arealdisponeringMedTomt);

            utnyttingsgrad = utnyttelsesgradRequestProcessing.percentageCalculation(arealdisponeringMedTomt, Constants.METHOD_TILLATT_TOMTEUTNYTTELSE);

            if (utnyttingsgrad.statusKode == Utnyttingsgrad.UTNYTTINGSGRAD_SUCCESS)
            {
                utnyttingsgrad.resultatBeskrivelse = Constants.resultatBeskrivelseTillattTomteutnyttelse;
                Log.Information("{url} calculation completed with this result:", url);
            }
            else
            {
                Log.Information("{url} could not be completed ... returning an error:", url);
            }

            // Log the result variable
            Log.Information("{@utnyttingsgrad}", utnyttingsgrad);
            return utnyttingsgrad;

        }





        //------------------------------------------------------------------------------------------------------------------------------------
        //     PostGolvarealDeltPaaGrunnareal
        //------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Utnyttelsesgrad beregnet med metode brutto golvareal/brutto grunnareal, some forholdstall, ikke avrundet. Reguleringsplan 1969 - 1987 
        /// </summary>
        /// <param name="arealDisponeringMedTomtUtenParkering"></param>
        /// <returns></returns>
        // POST api/Utnyttelsesgrad/GolvarealDeltPaaGrunnareal
        [ActionName("GolvarealDeltPaaGrunnareal")]
        public Utnyttingsgrad PostGolvarealDeltPaaGrunnareal([FromBody] ArealDisponeringMedTomtUtenParkering arealDisponeringMedTomtUtenParkering)
        {
            string url = "api/Utnyttelsesgrad/GolvarealDeltPaaGrunnareal";

            UtnyttelsesgradRequestProcessing utnyttelsesgradRequestProcessing = new UtnyttelsesgradRequestProcessing();
            Utnyttingsgrad utnyttingsgrad = new Utnyttingsgrad();
            ArealdisponeringMedTomt arealdisponeringMedTomt = new ArealdisponeringMedTomt();

            // Fitting object ArealDisponeringMedTomtUtenParkering into an object ArealdisponeringMedTomt where the parking prameters have been zeroed out
            // RE-using the Process/Calculaiton service to calculate the utnyttelsesgrad 
            arealdisponeringMedTomt.tomtearealByggeomraade = arealDisponeringMedTomtUtenParkering.tomtearealByggeomraade;
            arealdisponeringMedTomt.arealBebyggelseEksisterende = arealDisponeringMedTomtUtenParkering.arealBebyggelseEksisterende;
            arealdisponeringMedTomt.arealBebyggelseSomSkalRives = arealDisponeringMedTomtUtenParkering.arealBebyggelseSomSkalRives;
            arealdisponeringMedTomt.arealBebyggelseNytt = arealDisponeringMedTomtUtenParkering.arealBebyggelseNytt;
            arealdisponeringMedTomt.antallParkeringsplasserTerreng = 0;
            arealdisponeringMedTomt.kravKvmPrParkeringsplass = 0;


            Log.Information("*** {url} called with these parameters:", url);
            Log.Information("{@arealDisponeringMedTomtUtenParkering}", arealDisponeringMedTomtUtenParkering);
            Log.Information("Transformed the data into this object:", url);
            Log.Information("{@arealdisponeringMedTomt}", arealdisponeringMedTomt);

            utnyttingsgrad = utnyttelsesgradRequestProcessing.percentageCalculation(arealdisponeringMedTomt, Constants.METHOD_GOLVAREALDELTPAAGRUNNAREAL);

            if (utnyttingsgrad.statusKode == Utnyttingsgrad.UTNYTTINGSGRAD_SUCCESS)
            {
                utnyttingsgrad.resultatBeskrivelse = Constants.resultatBeskrivelseBruttoGolvOverGrunn;
                Log.Information("{url} calculation completed with this result:", url);
            }
            else
            {
                Log.Information("{url} could not be completed ... returning an error:", url);
            }

            // Log the result variable
            Log.Information("{@utnyttingsgrad}", utnyttingsgrad);
            return utnyttingsgrad;

        }

    } // API controller
} // namespace
