﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DIBK.FellestjenesterBygg.Utnyttelsesgrad.Models
{
    /// <summary>
    ///  Input objekt for utregning av Utnyttelsesgrad der resultat er et areal
    /// </summary>
    public class ArealdisponeringUtenTomt
    {
        /// <summary>
        /// Areal av eksisterende bebyggelse, m&#178;
        /// </summary>
        public double arealBebyggelseEksisterende { get; set; }

        /// <summary>
        /// Areal av bebyggelse som skal rives, m&#178;
        /// </summary>
        public double arealBebyggelseSomSkalRives { get; set; }

        /// <summary>
        /// Areal av ny bebyggelse, m&#178;
        /// </summary>
        public double arealBebyggelseNytt { get; set; }

        /// <summary>
        /// Antall parkeringsplasser (biloppstillingsplasser) i terreng    
        /// </summary>
        public int antallParkeringsplasserTerreng { get; set; }

        /// <summary>
        /// Kvadratmeter krav per parkeringsplass, m&#178;
        /// </summary>
        public double kravKvmPrParkeringsplass { get; set; }

    }
}