﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DIBK.FellestjenesterBygg.Utnyttelsesgrad.Models
{
    /// <summary>
    /// Resultat objekt for utregning. 
    /// </summary>
    public class Utnyttingsgrad
    {
        /// <summary>
        /// statusInt returned if calculation was succesfull
        /// </summary>
        public const int UTNYTTINGSGRAD_SUCCESS = 0;

        /// <summary>
        /// statusInt returned if calculation failed
        /// </summary>
        public const int UTNYTTINGSGRAD_ERROR = 1;

        /// <summary>
        /// Status kode for funksjon. Returnerer 0 for suksess og 1 for feil.
        /// </summary>
        public int  statusKode { get; set; }
        
        /// <summary>
        /// Beskrivelse av resultat eller mer utfyllende feil kode.
        /// </summary>
        public string resultatBeskrivelse { get; set; }

        /// <summary>
        /// Resultrat at utregning
        /// </summary>
        public double beregnetGradAvUtnytting { get; set; }

        /// <summary>
        /// Enhet for utregning
        /// </summary>
        public string enhetForBeregning { get; set; }

    }
}