﻿using System.Web.Configuration;
using System.Web.Mvc;

namespace KommIT.FIKS.AdapterAltinnSvarUt.Models
{
    public static class HtmlHelperExtensions
    {
        public static string ApplicationVersionNumber(this HtmlHelper helper)
        {
            return WebConfigurationManager.AppSettings["AppVersionNumber"];
        }
    }
}