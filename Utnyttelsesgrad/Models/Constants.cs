﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DIBK.FellestjenesterBygg.Utnyttelsesgrad.Services
{
    /// <summary>
    /// Constants for internal use
    /// </summary>
    public static class Constants
    {

        // Method descriptions 
        public static int METHOD_BEBYGDAREAL_BYA { get; } = 0;
        public static int METHOD_PROSENTBEBYGDAREAL_pctBYA { get; } = 1;
        public static int METHOD_BRUKSAREAL_BRA { get; } = 2;
        public static int METHOD_PROSENTBRUKSAREAL_pctBRA { get; } = 3;
        public static int METHOD_PROSENTTOMTEUTNYTTELSE_pctBRA { get; } = 4;
        public static int METHOD_TILLATT_BEBYGDAREAL { get; } = 5;
        public static int METHOD_TILLATT_BRUKSAREAL { get; } = 6;
        public static int METHOD_TILLATT_TOMTEUTNYTTELSE { get; } = 7;
        public static int METHOD_GOLVAREALDELTPAAGRUNNAREAL { get; } = 8;
        public static int METHOD_TILLATT_BRUKSAREAL_TBRA { get; } = 9;

        // Calculation directives
        public static bool resultIsRoundedUp { get; } = true;
        public static bool resultIsNotRounded { get; } = false;
        public static bool resultIsAPercentage { get; } = true;
        public static bool resultIsARatio { get; } = false;



        // Resultats beskrivelse
        public static string resultatBeskrivelsePctBYA { get; } = "Utregnet %-BYA";
        public static string resultatBeskrivelsePctBRA { get; } = "Utregnet %-BRA";
        public static string resultatBeskrivelsePctTU { get; } = "Utregnet % TU";
        public static string resultatBeskrivelseBYA { get; } = "Utregnet m2-BYA";
        public static string resultatBeskrivelseBRA { get; } = "Utregnet m2-BRA";
        public static string resultatBeskrivelseTBRA { get; } = "Utregnet T-BRA";
        public static string resultatBeskrivelseTillattBebygdAreal { get; } = "Utregnet tillatt bebygd areal";
        public static string resultatBeskrivelseTillattTomteutnyttelse { get; } = "Utregnet tillatt tomteutnyttelse";
        public static string resultatBeskrivelseTillattBruksareal { get; } = "Utregnet tillatt bruksareal";
        public static string resultatBeskrivelseBruttoGolvOverGrunn { get; } = "Utregnet forholdstall av brutto golvareal/brutto grunnareal";



        // Error messages
        public static string status_ok { get; } = "OK";
        public static string status_param_error { get; } = "Parameter feil";
        public static string status_div_by_zero { get; } = "Dele med null feil";
        public static string status_general_error { get; } = "Generell feil";

        // info messages
        public static string info_calc_pctBYA { get; } = "Generell feil";


        public static string returned_unit_m2 { get; } = "m2";
        public static string returned_unit_percent { get; } = "%";
        public static string returned_unit_ratio { get; } = "";


        // EXCEPTIONS
        public static string exception_zeroValue_tomtearealByggeomraade { get; } = "Parameteren tomtearealByggeomraade har null verdi";
        public static string exception_parameter_negative_tomtearealByggeomraade { get; } = "Parametern tomtearealByggeomraade har en negative verdi";
        public static string exception_parameter_negative_arealBebyggelseEksisterende { get; } = "Parametern arealBebyggelseEksisterende har en negative verdi";
        public static string exception_parameter_negative_arealBebyggelseSomSkalRives { get; } = "Parametern arealBebyggelseSomSkalRives har en negative verdi";
        public static string exception_parameter_negative_arealBebyggelseNytt { get; } = "Parametern arealBebyggelseNytt har en negative verdi";
        public static string exception_parameter_negative_antallParkeringsplasserTerreng { get; } = "Parametern antallParkeringsplasserTerreng har en negative verdi";
        public static string exception_parameter_negative_kravKvmPrParkeringsplass { get; } = "Parametern kravKvmPrParkeringsplass har en negative verdi";
        public static string exception_result_is_infinite  { get; } = "Beregnet beregnetGradAvUtnytting er Infinte";
        public static string exception_result_is_negative { get; } = "Beregnet beregnetGradAvUtnytting er et negativt nummer. Kontroller input parameterene";
       

    }
}