﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DIBK.FellestjenesterBygg.Utnyttelsesgrad.Models
{
    /// <summary>
    /// Input objekt for utregning av Utnyttelsesgrad for Byggeforskrifter 1969–1979, ingen parkerings beregning
    /// </summary>
    public class ArealDisponeringMedTomtUtenParkering
    {
        /// <summary>
        /// Totalt areal av byggeområdet, m&#178;
        /// </summary>
        public double tomtearealByggeomraade { get; set; }

        /// <summary>
        /// Areal av eksisterende bebyggelse, m&#178;
        /// </summary>
        public double arealBebyggelseEksisterende { get; set; }

        /// <summary>
        /// Areal av bebyggelse som skal rives, m&#178;
        /// </summary>
        public double arealBebyggelseSomSkalRives { get; set; }

        /// <summary>
        /// Areal av ny bebyggelse, m&#178;
        /// </summary>
        public double arealBebyggelseNytt { get; set; }

    }
}