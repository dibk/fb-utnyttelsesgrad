﻿using DIBK.FellestjenesterBygg.Utnyttelsesgrad.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Serilog;


namespace DIBK.FellestjenesterBygg.Utnyttelsesgrad.Services
{
    public class UtnyttelsesgradCalculations
    {

        /// <summary>
        /// beregnetGradAvUtnytting_Method_percentage, brukes for beregninger som ender i prosent resultat
        /// </summary>
        /// <param name="arealdisponering"></param>
        /// <param name="isResultRounded"></param>
        /// <returns></returns>
        public double beregnetGradAvUtnytting_Method_percentage(ArealdisponeringMedTomt arealdisponering, bool isResultRounded, bool isPercentage)
        {

            // Calculation
            // arealBebyggelseEksisterende - arealBebyggelseSomSkalRives + arealBebyggelseNytt + (antallParkeringsplasserTerreng * kravKvmPrParkeringsplass)
            //---------------------------------------------------------------------------------------------------------------------------------------------
            //                                                        tomtearealByggeomraade

            // Function is checked for Infinte results, negative results, 


            double gradAvUtnytting = Double.NaN;

            try
            {
                // Calculate
                gradAvUtnytting = (arealdisponering.arealBebyggelseEksisterende
                                        - arealdisponering.arealBebyggelseSomSkalRives
                                        + arealdisponering.arealBebyggelseNytt
                                        + (arealdisponering.antallParkeringsplasserTerreng * arealdisponering.kravKvmPrParkeringsplass)
                                        ) / arealdisponering.tomtearealByggeomraade;


                // Check that the calculated number is not infinite
                if (double.IsInfinity(gradAvUtnytting))
                {
                    throw new ArgumentException(Constants.exception_result_is_infinite);
                }

                // Check that the calculated number is not negative
                if (gradAvUtnytting < 0)
                {
                    throw new ArgumentException(Constants.exception_result_is_negative);
                }


                // Adjust the number by
                //  - multiplying with 100 to get to a percenteage (%) 
                //  - Math.Ceiling to round up to the next decimal
                // Veiledning, Grad av utnytting Beregnings- og måleregler, Publikasjonskode: H-2300 B, Page 24 
                if (isPercentage)
                {
                    gradAvUtnytting = 100.0 * gradAvUtnytting;
                }

                if (isResultRounded)
                {
                    gradAvUtnytting = Math.Ceiling(gradAvUtnytting);
                }

            }
            catch (DivideByZeroException dbze)
            {
                // This exception will probably not ever throw since gradAvUtnytting is a double and it will 
                // take an Infinite number value rather than throw the DivideByZero exception
                throw dbze;
            }
            catch (ArgumentException ae)
            {
                // Argument exception for Infinte or negative number is thrown up to the calling function
                throw ae;
            }
            catch (Exception e)
            {
                // Unexpected exception handler
                throw e;
            }

            return gradAvUtnytting;
        }


        /// <summary>
        ///   Calculates gradAvUtnytting with parking consideration
        ///   Result is reported in kvm or m2 rounded through ceiling function to calcualte an upper bound whole number
        ///   Veilending states øvre grense for BRA and BYA
        /// </summary>
        /// <param name="arealdisponering"></param>
        /// <param name="isResultRounded"></param>
        /// <returns></returns>
        public double beregnetGradAvUtnytting_Method_areaWithParking(ArealdisponeringUtenTomt arealdisponering, bool isResultRounded)
        {



            // Basic area calculation:
            // arealBebyggelseEksisterende - arealBebyggelseSomSkalRives + arealBebyggelseNytt + (antallParkeringsplasserTerreng * kravKvmPrParkeringsplass)

            // Function is checked for Infinte results, negative results, 


            double gradAvUtnytting = Double.NaN;

            try
            {
                // Calculate
                gradAvUtnytting = (arealdisponering.arealBebyggelseEksisterende
                                        - arealdisponering.arealBebyggelseSomSkalRives
                                        + arealdisponering.arealBebyggelseNytt
                                        + (arealdisponering.antallParkeringsplasserTerreng * arealdisponering.kravKvmPrParkeringsplass)
                                        );


                // Check that the calculated number is not infinite
                if (double.IsInfinity(gradAvUtnytting))
                {
                    throw new ArgumentException(Constants.exception_result_is_infinite);
                }

                // Check that the calculated number is not negative
                if (gradAvUtnytting < 0)
                {
                    throw new ArgumentException(Constants.exception_result_is_negative);
                }


                // Adjust the number by
                //  - Math.Ceiling to round up to the next higher whole number
                // Veiledning, Grad av utnytting Beregnings- og måleregler, Publikasjonskode: H-2300 B, refernces to  øvre grense
                // Does not apply to older standards, so a parameter is passed to the function to specify rounding
                if (isResultRounded)
                {
                    gradAvUtnytting = Math.Ceiling(gradAvUtnytting);
                }
            }
            catch (DivideByZeroException dbze)
            {
                // This exception will probably not ever throw since gradAvUtnytting is a double and it will 
                // take an Infinite number value rather than throw the DivideByZero exception
                throw dbze;
            }
            catch (ArgumentException ae)
            {
                // Argument exception for Infinte or negative number is thrown up to the calling function
                throw ae;
            }
            catch (Exception e)
            {
                // Unexpected exception handler
                throw e;
            }

            return gradAvUtnytting;
        }




    } // class
} // namespace