﻿using DIBK.FellestjenesterBygg.Utnyttelsesgrad.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace DIBK.FellestjenesterBygg.Utnyttelsesgrad.Services
{
    public class UtnyttelsesgradInputValidations
    {

        public void numericalsCheckedForNegativeValues(ArealdisponeringMedTomt arealdisponeringMedTomt)
        {

            // Value Checks
            //    tomtearealByggeomraade,           double, cannot be negative
            //    arealBebyggelseEksisterende,      double, cannot be negative
            //    realBebyggelseSomSkalRives,       double, cannot be negative
            //    arealBebyggelseNytt,              double, cannot be negative
            //    antallParkeringsplasserTerreng,   int,    cannot be negative
            //    kravKvmPrParkeringsplass,         double, cannot be negative

            if (arealdisponeringMedTomt.tomtearealByggeomraade < 0)
            {
                throw new ArgumentException(Constants.exception_parameter_negative_tomtearealByggeomraade);
            }
            if (arealdisponeringMedTomt.arealBebyggelseEksisterende < 0)
            {
                throw new ArgumentException(Constants.exception_parameter_negative_arealBebyggelseEksisterende);
            }
            if (arealdisponeringMedTomt.arealBebyggelseSomSkalRives < 0)
            {
                throw new ArgumentException(Constants.exception_parameter_negative_arealBebyggelseSomSkalRives);
            }
            if (arealdisponeringMedTomt.arealBebyggelseNytt < 0)
            {
                throw new ArgumentException(Constants.exception_parameter_negative_arealBebyggelseNytt);
            }
            if (arealdisponeringMedTomt.antallParkeringsplasserTerreng < 0)
            {
                throw new ArgumentException(Constants.exception_parameter_negative_antallParkeringsplasserTerreng);
            }
            if (arealdisponeringMedTomt.kravKvmPrParkeringsplass < 0)
            {
                throw new ArgumentException(Constants.exception_parameter_negative_kravKvmPrParkeringsplass);
            }
        }


        public void numericalsCheckedForNegativeValues(ArealdisponeringUtenTomt arealdisponeringUtenTomt)
        {

            // Value Checks
            //    arealBebyggelseEksisterende,      double, cannot be negative
            //    realBebyggelseSomSkalRives,       double, cannot be negative
            //    arealBebyggelseNytt,              double, cannot be negative
            //    antallParkeringsplasserTerreng,   int,    cannot be negative
            //    kravKvmPrParkeringsplass,         double, cannot be negative

            if (arealdisponeringUtenTomt.arealBebyggelseEksisterende < 0)
            {
                throw new ArgumentException(Constants.exception_parameter_negative_arealBebyggelseEksisterende);
            }
            if (arealdisponeringUtenTomt.arealBebyggelseSomSkalRives < 0)
            {
                throw new ArgumentException(Constants.exception_parameter_negative_arealBebyggelseSomSkalRives);
            }
            if (arealdisponeringUtenTomt.arealBebyggelseNytt < 0)
            {
                throw new ArgumentException(Constants.exception_parameter_negative_arealBebyggelseNytt);
            }
            if (arealdisponeringUtenTomt.antallParkeringsplasserTerreng < 0)
            {
                throw new ArgumentException(Constants.exception_parameter_negative_antallParkeringsplasserTerreng);
            }
            if (arealdisponeringUtenTomt.kravKvmPrParkeringsplass < 0)
            {
                throw new ArgumentException(Constants.exception_parameter_negative_kravKvmPrParkeringsplass);
            }
        }



        public void tomtearealByggeomraadeCheckedForZeroValue(ArealdisponeringMedTomt arealdisponeringMedTomt)
        {

            // Value Checks
            //    tomtearealByggeomraade, double, cannot be zero

            if (arealdisponeringMedTomt.tomtearealByggeomraade == 0)
            {
                throw new ArgumentException(Constants.exception_zeroValue_tomtearealByggeomraade);
            }
        }



    }
}
 