﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DIBK.FellestjenesterBygg.Utnyttelsesgrad.Models;
using Serilog;


namespace DIBK.FellestjenesterBygg.Utnyttelsesgrad.Services
{
    internal class UtnyttelsesgradRequestProcessing
    {

        internal Utnyttingsgrad percentageCalculation(ArealdisponeringMedTomt arealdisponeringMedTomt, int calculationMethod)
        {

            // Instantiations and local variables
            UtnyttelsesgradCalculations utnyttelsesgradCalculations = new UtnyttelsesgradCalculations();
            UtnyttelsesgradInputValidations utnyttelsesgradInputValidations = new UtnyttelsesgradInputValidations();
            Utnyttingsgrad return_object_utnyttingsgrad = new Utnyttingsgrad();

            try
            {


                // Validate input paramaters, throws ArgumentException exception if negative value is found
                utnyttelsesgradInputValidations.numericalsCheckedForNegativeValues(arealdisponeringMedTomt);

                // Validate input paramaters, throws ArgumentException exception if zero value is found
                utnyttelsesgradInputValidations.tomtearealByggeomraadeCheckedForZeroValue(arealdisponeringMedTomt);

                // Decide if rounding is used or not
                if (calculationMethod == Constants.METHOD_GOLVAREALDELTPAAGRUNNAREAL)
                {
                    // Calcualte data
                    return_object_utnyttingsgrad.beregnetGradAvUtnytting =
                        utnyttelsesgradCalculations.beregnetGradAvUtnytting_Method_percentage(arealdisponeringMedTomt, Constants.resultIsNotRounded, Constants.resultIsARatio);
                    // Define the unit for the calculation
                    return_object_utnyttingsgrad.enhetForBeregning = Constants.returned_unit_ratio;

                }
                else if ( calculationMethod == Constants.METHOD_TILLATT_BEBYGDAREAL || calculationMethod == Constants.METHOD_TILLATT_TOMTEUTNYTTELSE)
                {
                    // Calcualte data
                    return_object_utnyttingsgrad.beregnetGradAvUtnytting = 
                        utnyttelsesgradCalculations.beregnetGradAvUtnytting_Method_percentage(arealdisponeringMedTomt, Constants.resultIsNotRounded, Constants.resultIsAPercentage);
                    // Define the unit for the calculation
                    return_object_utnyttingsgrad.enhetForBeregning = Constants.returned_unit_percent;

                }
                else
                {
                    // Calcualte data
                    return_object_utnyttingsgrad.beregnetGradAvUtnytting = 
                        utnyttelsesgradCalculations.beregnetGradAvUtnytting_Method_percentage(arealdisponeringMedTomt, Constants.resultIsRoundedUp, Constants.resultIsAPercentage);
                    // Define the unit for the calculation
                    return_object_utnyttingsgrad.enhetForBeregning = Constants.returned_unit_percent;

                }

                // Return calculated data
                return_object_utnyttingsgrad.statusKode = Utnyttingsgrad.UTNYTTINGSGRAD_SUCCESS;


            }
            catch (ArgumentException ae)
            {
                Log.Error("Arealdisponering, ArgumentException: " + ae.ToString());
                // Populate return object
                return_object_utnyttingsgrad.statusKode = Utnyttingsgrad.UTNYTTINGSGRAD_ERROR;
                return_object_utnyttingsgrad.resultatBeskrivelse = Constants.status_param_error + ": " + ae.Message.ToString();
                return_object_utnyttingsgrad.beregnetGradAvUtnytting = Double.NaN;
            }
            catch (DivideByZeroException dbze)
            {
                Log.Error("Arealdisponering, DivideByZeroException: " + dbze.ToString());
                // Populate return object
                return_object_utnyttingsgrad.statusKode = Utnyttingsgrad.UTNYTTINGSGRAD_ERROR;
                return_object_utnyttingsgrad.resultatBeskrivelse = Constants.status_div_by_zero + ": " + dbze.Message.ToString();
                return_object_utnyttingsgrad.beregnetGradAvUtnytting = Double.NaN;
            }
            catch (Exception e)
            {
                Log.Error("Arealdisponering, General Exception: " + e.ToString());
                // Populate return object
                return_object_utnyttingsgrad.statusKode = Utnyttingsgrad.UTNYTTINGSGRAD_ERROR;
                return_object_utnyttingsgrad.resultatBeskrivelse = Constants.status_general_error + ": " + e.ToString();
                return_object_utnyttingsgrad.beregnetGradAvUtnytting = Double.NaN;
            }


            // TODO: update documentation Return "beregnetGradAvUtnytting" type="xs:double"
            return return_object_utnyttingsgrad;
        }


        internal Utnyttingsgrad areaCalculation(ArealdisponeringUtenTomt arealdisponeringUtenTomt, int calculationMethod)
        {

            // Instantiations and local variables
            UtnyttelsesgradCalculations utnyttelsesgradCalculations = new UtnyttelsesgradCalculations();
            UtnyttelsesgradInputValidations utnyttelsesgradInputValidations = new UtnyttelsesgradInputValidations();
            Utnyttingsgrad return_object_utnyttingsgrad = new Utnyttingsgrad();

            try
            {

                // Define the unit for the calculation
                return_object_utnyttingsgrad.enhetForBeregning = Constants.returned_unit_m2;

                // Validate input paramaters, throws ArgumentException exception if negative value is found
                utnyttelsesgradInputValidations.numericalsCheckedForNegativeValues(arealdisponeringUtenTomt);

                // Calcualte data depending on weather 
                if (calculationMethod == Constants.METHOD_TILLATT_BRUKSAREAL)
                {
                    return_object_utnyttingsgrad.beregnetGradAvUtnytting = 
                        utnyttelsesgradCalculations.beregnetGradAvUtnytting_Method_areaWithParking(arealdisponeringUtenTomt, Constants.resultIsNotRounded);
                }
                else
                {
                    return_object_utnyttingsgrad.beregnetGradAvUtnytting =
                        utnyttelsesgradCalculations.beregnetGradAvUtnytting_Method_areaWithParking(arealdisponeringUtenTomt, Constants.resultIsRoundedUp);

                }
                return_object_utnyttingsgrad.statusKode = Utnyttingsgrad.UTNYTTINGSGRAD_SUCCESS;


            }
            catch (ArgumentException ae)
            {
                Log.Error("Arealdisponering, ArgumentException: " + ae.ToString());
                // Populate return object
                return_object_utnyttingsgrad.statusKode = Utnyttingsgrad.UTNYTTINGSGRAD_ERROR;
                return_object_utnyttingsgrad.resultatBeskrivelse = Constants.status_param_error + ": " + ae.Message.ToString();
                return_object_utnyttingsgrad.beregnetGradAvUtnytting = Double.NaN;
            }
            catch (DivideByZeroException dbze)
            {
                Log.Error("Arealdisponering, DivideByZeroException: " + dbze.ToString());
                // Populate return object
                return_object_utnyttingsgrad.statusKode = Utnyttingsgrad.UTNYTTINGSGRAD_ERROR;
                return_object_utnyttingsgrad.resultatBeskrivelse = Constants.status_div_by_zero + ": " + dbze.Message.ToString();
                return_object_utnyttingsgrad.beregnetGradAvUtnytting = Double.NaN;
            }
            catch (Exception e)
            {
                Log.Error("Arealdisponering, General Exception: " + e.ToString());
                // Populate return object
                return_object_utnyttingsgrad.statusKode = Utnyttingsgrad.UTNYTTINGSGRAD_ERROR;
                return_object_utnyttingsgrad.resultatBeskrivelse = Constants.status_general_error + ": " + e.ToString();
                return_object_utnyttingsgrad.beregnetGradAvUtnytting = Double.NaN;
            }


            // TODO: update documentation Return "beregnetGradAvUtnytting" type="xs:double"
            return return_object_utnyttingsgrad;
        }



    } // class
} // namespace